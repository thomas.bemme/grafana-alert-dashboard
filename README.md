# Grafana Alert Dashboard

Dashboard to display alerts defined in Prometheus including general overview, current status and alert history.

https://grafana.com/grafana/dashboards/16420
